<?php session_start();



        require('config.php');
        $select = $bdd->query("SELECT * FROM connection");
        


   
    
?>


<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>Elite</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="shortcut icon" type="image/x-icon" href="img/logo.ico">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">
    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
  	<div class="py-1 bg-black top">
    	<div class="container">
    		<div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
	    		<div class="col-lg-12 d-block">
		    		<div class="row d-flex">
		    			<div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
						    <span class="text">+ 112 en cas d'urgence</span>
					    </div>					   
					    <?php 
    	if(isset($_COOKIE['password'])){ }else{ echo "  <div class='col-md-5 pr-4 d-flex topper align-items-center text-lg-right justify-content-end'>
					     <p class='mb-0 register-link'><a href='enregistrement.php' class='mr-3'>S'inscrire</a>
					                                   <a href='Acceuil.php' class='mr-3'>Se connecter</a></p>
					    </div>";}?>
		
				    </div>
			    </div>
		    </div>
		  </div>



    </div>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light site-navbar-target" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="index.php">ELITE CORP</a>
	      <button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav nav ml-auto">
	  
	          <li class="nav-item"><a href="index.php#about-section" class="nav-link"><span>A propos</span></a></li>
	          <li class="nav-item"><a href="index.php#department-section" class="nav-link"><span>Departement</span></a></li>
	          <li class="nav-item"><a href="index.php#doctor-section" class="nav-link"><span>Docteurs</span></a></li>
	  
	          <li class="nav-item"><a href="index.php#contact-section" class="nav-link"><span>Contact</span></a></li>
	           <li class="nav-item"><a href="planning.php" class="nav-link">Planning</a></li>
	           <?php 
    	if(isset($_COOKIE['password'])){ echo "<li class='nav-item cta mr-md-2'><a href='reservation.php' class='nav-link'>Rendez-vous</a></li>
	          <li class='nav-item' ><a href='session.php' class='nav-link'><span>";}?>
	          <?php if(isset($_COOKIE['password'])){echo "Bonjour ".$_COOKIE['pseudo'];} ?></span></a></li>
	        </ul>
	      </div>
	    </div>
	  </nav>
	  
	  <section class="hero-wrap js-fullheight" style="background-image: url('images/bg_3.jpg');" data-section="home" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
          <div class="col-md-6 pt-5 ftco-animate">
          	<div class="mt-5">
          		<span class="subheading">Bienvenue sur ELITE CORP</span>
	            <h1 class="mb-4">Nous sommes ici <br>pour votre santé </h1>
	            <p class="mb-4">ELITE CORP vous propose les meilleurs soin pour vous et fairas tous son possible pour que vous rentrez en pleine forme</p>
	            <p><a href="reservation.php" class="btn btn-primary py-3 px-4">Prendre un rendez-vous</a></p>
            </div>
          </div>
        </div>
      </div>
    </section>

		<section class="ftco-counter img ftco-section ftco-no-pt ftco-no-pb" id="about-section">
    	<div class="container">
    		<div class="row d-flex">
    			<div class="col-md-6 col-lg-5 d-flex">
    				<div class="img d-flex align-self-stretch align-items-center" style="background-image:url(images/about.jpg);">
    				</div>
    			</div>
    			<div class="col-md-6 col-lg-7 pl-lg-5 py-md-5">
    				<div class="py-md-5">
	    				<div class="row justify-content-start pb-3">
			          <div class="col-md-12 heading-section ftco-animate p-4 p-lg-5">
			            <h2 class="mb-4">Nous sommes <span>ELITE CORP</span> un regroupement de clinique</h2>
			            <p>Nous regroupons des clinique a travers toute la France ce qui nous permet de facilité les démarche entre confrère </p>
			            <p><a href="reservation.php" class="btn btn-primary py-3 px-4">Prendre un rendez-vous</a> <a href="#contact-section" class="btn btn-secondary py-3 px-4">Contacter nous</a></p>
			          </div>
			        </div>
		        </div>
	        </div>
        </div>
    	</div>
    </section>


		<section class="ftco-section ftco-no-pt ftco-no-pb ftco-services-2 bg-light" id="options">
			<div class="container">
        <div class="row d-flex">
	        <div class="col-md-7 py-5">
	        	<div class="py-lg-5">
		        	<div class="row justify-content-center pb-5">
			          <div class="col-md-12 heading-section ftco-animate">
			            <h2 class="mb-3">Nos Services</h2>
			          </div>
			        </div>
			        <div class="row">
			        	<div class="col-md-6 d-flex align-self-stretch ftco-animate">
			            <div class="media block-6 services d-flex">
			              <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-ambulance"></span></div>
			              <div class="media-body pl-md-4">
			                <h3 class="heading mb-3">Services d'Urgences</h3>
			                <p>Nous pouvons vous mettre a disposition une embulance pour venir vous chercher</p>
			              </div>
			            </div>      
			          </div>
			          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
			            <div class="media block-6 services d-flex">
			              <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-doctor"></span></div>
			              <div class="media-body pl-md-4">
			                <h3 class="heading mb-3">Docteurs Qualifiés</h3>
			                <p>Nous avons pris l'ELITE de l'ELITE pour nos docteurs car nous sommes les meilleurs</p>
			              </div>
			            </div>      
			          </div>
			          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
			            <div class="media block-6 services d-flex">
			              <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-stethoscope"></span></div>
			              <div class="media-body pl-md-4">
			                <h3 class="heading mb-3">Checkup a domicile </h3>
			                <p>Si vous etes pressée ou ne pouvée pas venir nous pouvons envoyée un médecin a domicile </p>
			              </div>
			            </div>      
			          </div>
			          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
			            <div class="media block-6 services d-flex">
			              <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-24-hours"></span></div>
			              <div class="media-body pl-md-4">
			                <h3 class="heading mb-3">Service ouverts 24/24 H</h3>
			                <p>Grace a nos nombre prestataire nous pouvons intervenir partout en France et ceux 24h / 24</p>
			              </div>
			            </div>      
			          </div>
			        </div>
			      </div>
		      </div>
		      

    <section class="ftco-intro img" style="background-image: url(images/bg_2.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-9 text-center">
						<h2>Votre santée notre prioritée</h2>
						<p>Sans vous nous ne serrons rien !</p>
						
					</div>
				</div>
			</div>
		</section>

		<section class="ftco-section ftco-no-pt ftco-no-pb" id="department-section">
    	<div class="container-fluid px-0">
    		<div class="row no-gutters">
    			<div class="col-md-4 d-flex">
    				<div class="img img-dept align-self-stretch" style="background-image: url(images/dept-1.jpg);"></div>
    			</div>

    			<div class="col-md-8">
    				<div class="row no-gutters">
    					<div class="col-md-4">
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a>Neurologie</a></h3>
    								<p> étudie le système nerveux et ses maladies.</p>
    							</div>
    						</div>
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a>Chirurgie</a></h3>
    								<p>Traite les blessures et les maladies par des moyens faisant appel à des opération</p>
    							</div>
    						</div>
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a>Dentaire</a></h3>
    								<p>Opération Dentaire</p>
    							</div>
    						</div>
    					</div>

    					<div class="col-md-4">
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a>Opthalmologie</a></h3>    								<p>raitement des maladies de l'œil et de ses annexes.  </p>
    							</div>
    						</div>
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a>Cardiologie</a></h3>
    								<p>est une branche de la médecine qui traite des troubles du cœur </p>
    							</div>
    						</div>
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a>Traumatologie</a></h3>
    								<p>Partie de la médecine, de la chirurgie consacrée à soigner les blessures, les suites d'accidents.</p>
    							</div>
    						</div>
    					</div>

    					<div class="col-md-4">
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a>IRM</a></h3>
    								<p>Une extension sans doute plus connue dans le grand public est l'imagerie par résonance magnétique (IRM)</p>
    							</div>
    						</div>
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a>rayon-X</a></h3>
    								<p> Radiographie, médecine nucléaire et radiothérapie</p>
    							</div>
    						</div>
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a>Kinésithérapie</a></h3>
    								<p>spécialistes de la rééducation fonctionnelle du corps</p>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
		
		<section class="ftco-section" id="doctor-section">
			<div class="container-fluid px-5">
				<div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-8 text-center heading-section ftco-animate">
            <h2 class="mb-4">Nos médecins qualifiés</h2>
            <p>Tout droit sorti des plus grandes universités reconnus du monde, nos médecins ce mettent généreusement à votre disposition pour un suivi total et complet </p>
          </div>
        </div>	
				<div class="row">
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="staff">
							<div class="img-wrap d-flex align-items-stretch">
								<div class="img align-self-stretch" style="background-image: url(images/clem.jpg);"></div>
							</div>
							<div class="text pt-3 text-center">
								<h3 class="mb-2">Dr. Clem Clem</h3>
								<span class="position mb-2">Neurologist</span>
								<div class="faded">
									<p>Etant follement passioné par mon boulot j'entretiens une relation fusionelle avec mes patients jusqu'à leurs ultime guérison</p>
									<ul class="ftco-social text-center">
		                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
		              </ul>
		              <p><a href="reservation.php" class="btn btn-primary">Réserver</a></p>
	              </div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="staff">
							<div class="img-wrap d-flex align-items-stretch">
								<div class="img align-self-stretch" style="background-image: url(images/ben.jpg);"></div>
							</div>
							<div class="text pt-3 text-center">
								<h3 class="mb-2">Dr. Bitoun</h3>
								<span class="position mb-2">Ophthalmologist</span>
								<div class="faded">
									<p>Je suis ambitieux accro du travail dévoué mais à part ça, je suis plutôt une simple personne avec le sourire</p><br>
									<ul class="ftco-social text-center">
		                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
		                <li class="ftco-animate"><a href="https://www.instagram.com/benjamin_2303/"><span class="icon-instagram"></span></a></li>
		              </ul>
		              <p><a href="reservation.php" class="btn btn-primary">Réserver</a></p>
	              </div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="staff">
							<div class="img-wrap d-flex align-items-stretch">
								<div class="img align-self-stretch" style="background-image: url(images/zineb.jpg);"></div>
							</div>
							<div class="text pt-3 text-center">
								<h3 class="mb-2">Dr.  Amanar</h3>
								<span class="position mb-2">Dentist</span>
								<div class="faded">
									<p>IJe suis un jeune dentiste éxperimenté dans mon travail depuis peu, acharné et ambitieux je serais ravis de vous prendre en charge</p>
									<ul class="ftco-social text-center">
		                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
		                <li class="ftco-animate"><a href="https://www.instagram.com/zineb.amn/"><span class="icon-instagram"></span></a></li>
		              </ul>
		              <p><a href="reservation.php" class="btn btn-primary">Réserver</a></p>
	              </div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="staff">
							<div class="img-wrap d-flex align-items-stretch">
								<div class="img align-self-stretch" style="background-image: url(images/milan.jpg);"></div>
							</div>
							<div class="text pt-3 text-center">
								<h3 class="mb-2">Dr. Djevansir</h3>
								<span class="position mb-2">Pediatrician</span>
								<div class="faded">
									<p>homme heureux dans sa profession qui ce donne à 100% jour et nuit</p><br><br>
									<ul class="ftco-social text-center">
		                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
		              </ul>
		              <p><a href="reservation.php" class="btn btn-primary">Réserver</a></p>
	              </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="ftco-facts img ftco-counter" style="background-image: url(images/bg_3.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row d-flex align-items-center">
					<div class="col-md-5 heading-section heading-section-white">
						<span class="subheading">faits intéressants</span>
						<h2 class="mb-4">Plus de 5 100 patients nous font confiance</h2>
						<p class="mb-0"><a href="reservation.php" class="btn btn-secondary px-4 py-3">Prendre rendez-vous</a></p>
					</div>
					<div class="col-md-7">
						<div class="row pt-4">
		          <div class="col-md-6 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18">
		              <div class="text">
		                <strong class="number" data-number="30">0</strong>
		                <span>Années d’expérience</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-6 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18">
		              <div class="text">
		                <strong class="number" data-number="4500">0</strong>
		                <span>Patients heureux</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-6 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18">
		              <div class="text">
		                <strong class="number" data-number="84">0</strong>
		                <span>Nombre de médecins</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-6 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18">
		              <div class="text">
		                <strong class="number" data-number="300">0</strong>
		                <span>Nombre de membres du personnel</span>
		              </div>
		            </div>
		          </div>
	          </div>
					</div>
				</div>
			</div>
		</section>


    <section class="ftco-section bg-light" id="blog-section" >
      <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
          <div class="col-md-10 heading-section text-center ftco-animate">
            <h2 class="mb-4">Toutes les mises à jour ici</h2>
            <p>Installer la dernière version 2.1 de notre site web elitecorp.com sans plus attendre</p>
          </div>
        </div>
        <div class="row d-flex">
          <div class="col-md-4 ftco-animate">
            <div class="blog-entry">
              <a href="blog-single.php" class="block-20" style="background-image: url('images/image_1.jpg');">
              </a>
              <div class="text d-block">
              	<div class="meta mb-3">
                  <div><a href="#">Le 5 Janvier 2020</a></div>
                  <div><a href="#">Admin</a></div>
                  <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                </div>
                <h3 class="heading"><a href="#">Nous signalez un problème</a></h3>
                <p>Si vous êtes insatisfait de nos services n'hésitez pas à nous faire part de votre éxperience pour que nous puissions régler ce problème au plus vite.</p>
                
              </div>
            </div>
        	</div>

        	<div class="col-md-4 ftco-animate">
            <div class="blog-entry">
              <a href="blog-single.php" class="block-20" style="background-image: url('images/image_2.jpg');">
              </a>
              <div class="text d-block">
              	<div class="meta mb-3">
                  <div><a href="#">Le 2 Janvier 2020</a></div>
                  <div><a href="#">Admin</a></div>
                  <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                </div>
                <h3 class="heading"><a href="#">Nous rejoindre</a></h3>
                <p>Vous êtes titulaire d'un bac+15 avec 60 ans d'experience alors qu'attendez vous pour nous rejoindre envoyer votre CV pour decrocher un entretien.</p>
                
              </div>
            </div>
        	</div>

        	<div class="col-md-4 ftco-animate">
            <div class="blog-entry">
              <a href="blog-single.php" class="block-20" style="background-image: url('images/image_3.jpg');">
              </a>
              <div class="text d-block">
              	<div class="meta mb-3">
                  <div><a href="#">Le 2 Janvier 2020</a></div>
                  <div><a href="#">Admin</a></div>
                  <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                </div>
                <h3 class="heading"><a href="#">Nous rejoindre </a></h3>
                <p>Vous êtes titulaire d'un bac+15 avec 60 ans d'experience alors qu'attendez vous pour nous rejoindre envoyer votre CV pour decrocher un entretien.</p>
                
              </div>
            </div>
        	</div>

        	<div class="col-md-4 ftco-animate">
            <div class="blog-entry">
              <a href="blog-single.php" class="block-20" style="background-image: url('images/image_4.jpg');">
              </a>
              <div class="text d-block">
              	<div class="meta mb-3">
                  <div><a href="#">Le 31 Mai 2019</a></div>
                  <div><a href="#">Admin</a></div>
                  <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                </div>
                <h3 class="heading"><a href="#">Notre Psychologie</a></h3>
                <p>Discuter avec nos psychologues est aussi une des démarches de guérison nous proposons des suivis tout les mois pour nos patients atteint de troubles mentals.</p>
                
              </div>
            </div>
        	</div>

        	<div class="col-md-4 ftco-animate">
            <div class="blog-entry">
              <a href="blog-single.php" class="block-20" style="background-image: url('images/image_5.jpg');">
              </a>
              <div class="text d-block">
              	<div class="meta mb-3">
                  <div><a href="#">15 Avril 2019</a></div>
                  <div><a href="#">Admin</a></div>
                  <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                </div>
                <h3 class="heading"><a href="#">Soins en Pédiatrie</a></h3>
                <p>Les enfants sont aussi très bien acceuillis et nous disposons de trois grands bâtiments reserver uniquement aux soins dédiés aux enfants.</p>
                
              </div>
            </div>
        	</div>

        	<div class="col-md-4 ftco-animate">
            <div class="blog-entry">
              <a href="blog-single.php" class="block-20" style="background-image: url('images/image_6.jpg');">
              </a>
              <div class="text d-block">
              	<div class="meta mb-3">
                  <div><a href="#">Le 9 Juin 2019</a></div>
                  <div><a href="#">Admin</a></div>
                  <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                </div>
                <h3 class="heading"><a href="#">Notre équipe médical</a></h3>
                <p>Disponible 24h/24 pour vous fournir des soins appropriés à chacun de nos patients en temps et en heure</p>
                
              </div>
            </div>
        	</div>
        </div>
      </div>
    </section>

    <section class="ftco-section testimony-section img" style="background-image: url(images/bg_3.jpg);">
    	<div class="overlay"></div>
      <div class="container">
        <div class="row justify-content-center pb-3">
          <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
          	<span class="subheading">lire les témoignages</span>
            <h2 class="mb-4">Nos patients disent</h2>
          </div>
        </div>
        <div class="row ftco-animate justify-content-center">
          <div class="col-md-12">
            <div class="carousel-testimony owl-carousel ftco-owl">
              <div class="item">
                <div class="testimony-wrap text-center py-4 pb-5">
                  <div class="user-img" style="background-image: url(images/person_1.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                  </div>
                  <div class="text px-4">
                    <p class="mb-4">J'ai procéder à deux opérations à la suite et cela n'a durer que 8 jours des services de qualité mais aussi rapide.</p>
                    <p class="name">Mael Kruger</p>
                    <span class="position">Patients</span>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap text-center py-4 pb-5">
                  <div class="user-img" style="background-image: url(images/person_2.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                  </div>
                  <div class="text px-4">
                    <p class="mb-4">Un centre médical que je recommande à tous pour leurs compétences rarissime</p>
                    <p class="name">Jeff Freshman</p>
                    <span class="position">Patients</span>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap text-center py-4 pb-5">
                  <div class="user-img" style="background-image: url(images/person_3.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                  </div>
                  <div class="text px-4">
                    <p class="mb-4">J'en suis sortis totalement guérit un vrais miracle en seulement 2 ans ma fièvre s'en est aller.</p>
                    <p class="name">Alain Perret</p>
                    <span class="position">Patients</span>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap text-center py-4 pb-5">
                  <div class="user-img" style="background-image: url(images/person_1.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                  </div>
                  <div class="text px-4">
                    <p class="mb-4">De très bon médecins attentioné malgré que j'ai du être amputé de mes deux jambes après une petite erreur médical.</p>
                    <p class="name">Arthur Enno</p>
                    <span class="position">Patients</span>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap text-center py-4 pb-5">
                  <div class="user-img" style="background-image: url(images/person_3.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                  </div>
                  <div class="text px-4">
                    <p class="mb-4">Un dentiste génial des soins approprié le seul et unique bémole ces que je ne peux me nourrire uniquement de soupe aux légumes.</p>
                    <p class="name">Alain Perret</p>
                    <span class="position">Patients</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section contact-section" id="contact-section">
      <div class="container">
      	<div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 heading-section text-center ftco-animate">
            <h2 class="mb-4">Nous contacter</h2>
       
          </div>
        </div>
        <div class="row d-flex contact-info mb-5">
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box p-4 text-center bg-light">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-map-signs"></span>
          		</div>
          		<h3 class="mb-4">Siège Social</h3>
	            <p> <a href="https://www.google.com/maps/place/8+Rue+de+Londres,+75009+Paris/@48.8773819,2.3278012,17z/data=!3m1!4b1!4m5!3m4!1s0x47e66e49f03cea3b:0x7ec826a12ef68bbc!8m2!3d48.8773819!4d2.3299899">8 Rue de Londres, 75009 Paris FRANCE</a></p>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box p-4 text-center bg-light">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-phone2"></span>
          		</div>
          		<h3 class="mb-4">Numéro de téléphone</h3>
	            <p><a href="tel://0623527198">+ 06 23 52 71 98</a></p>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box p-4 text-center bg-light">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-paper-plane"></span>
          		</div>
          		<h3 class="mb-4">Address E-mail</h3>
	            <p><a href="mailto:ELITEcorp@gmail.com">ELITEcorp@gmail.com</a></p>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box p-4 text-center bg-light">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-globe"></span>
          		</div>
          		<h3 class="mb-4">Site WEB</h3>
	            <p><a href="index.php">EliteCorp.com</a></p>
	          </div>
          </div>
        </div>    <script type="text/javascript">
function confirmation() {
var str = '"Etes-vous  sur de vouloire  le message ?"';
return confirm(str);
}
</script>
        <div class="row no-gutters block-9">

          <div class="col-md-6 order-md-last d-flex">
            <form action="reclamation.php" class="bg-light p-5 contact-form" method="POST">
              
              <div class="form-group">
                <input type="text" name="Sujet" class="form-control" placeholder="Sujet">
              </div>
              <div class="form-group">
                <textarea name="Message" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
              </div>
              <div class="form-group">
              	  <p><a href="chat.php" class="btn btn-primary py-3 px-4">Parler a des conseiller en direct </a></p>
                <input type="submit" value="Envoyée Message" class="btn btn-secondary py-3 px-5">
              </div>
            </form>
          
          </div>
          <div class="col-md-6 d-flex">
          	<div id="map" class="bg-white"></div>
          </div>
        </div>
      </div>
    </section>

    <footer class="ftco-footer ftco-section img" style="background-image: url(images/footer-bg.jpg);">
    	<div class="overlay"></div>
      <div class="container-fluid px-md-5">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">ELITE CORP</h2>
              <p>Depuis 30 ans ELITE CORP sont à vos soins et sont joignable via différents réseaux.</p>
              <ul class="ftco-footer-social list-unstyled mt-5">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-4">
              <h2 class="ftco-heading-2">Departements</h2>
              <ul class="list-unstyled">
                <li><a href="#department-section"><span class="icon-long-arrow-right mr-2"></span>Neurologie</a></li>
                <li><a href="#department-section"><span class="icon-long-arrow-right mr-2"></span>Opthalmologie</a></li>
                <li><a href="#department-section"><span class="icon-long-arrow-right mr-2"></span>IRM</a></li>
                <li><a href="#department-section"><span class="icon-long-arrow-right mr-2"></span>chirurgie</a></li>
                <li><a href="#department-section"><span class="icon-long-arrow-right mr-2"></span>Cardiology</a></li>
                <li><a href="#department-section"><span class="icon-long-arrow-right mr-2"></span>Dentaire</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-4">
              <h2 class="ftco-heading-2">Liens</h2>
              <ul class="list-unstyled">
                <li><a href="index.php"><span class="icon-long-arrow-right mr-2"></span>Acceuil</a></li>
                <li><a href="#about-section"><span class="icon-long-arrow-right mr-2"></span>A propos</a></li>
                <li><a href="#department-section"><span class="icon-long-arrow-right mr-2"></span>Departments</a></li>
                <li><a href="#doctor-section"><span class="icon-long-arrow-right mr-2"></span>Docteurs</a></li>
             
                <li><a href="reservation.php"><span class="icon-long-arrow-right mr-2"></span>Rendez-vous</a></li>
                <li><a href="#contact-section"><span class="icon-long-arrow-right mr-2"></span>Contact</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Services</h2>
              <ul class="list-unstyled">
                <li><a href="https://www.samu-urgences-de-france.fr/fr/"><span class="icon-long-arrow-right mr-2"></span>Services d'Urgence</a></li>
                <li><a href="#doctor-section"><span class="icon-long-arrow-right mr-2"></span>Médecins qualifiés</a></li>
                <li><a href="#options"><span class="icon-long-arrow-right mr-2"></span>Examen à dommcile </a></li>
                <li><a href="#options"><span class="icon-long-arrow-right mr-2"></span>24H /24 7j/7  Services</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Des Questions ?</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><a ><span class="icon icon-map-marker"></span><span class="text">8 Rue de Londres, 75009 Paris FRANCE</span></a></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text">+ 06 23 52 71 98</span></a></li>
	                <li><a href="#"><span class="icon icon-envelope pr-4"></span><span class="text">ELITEcorp@gmail.com</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
	
           
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  
  <script src="js/main.js"></script>
    
  </body>
</html>