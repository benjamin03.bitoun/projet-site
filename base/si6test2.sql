-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  ven. 10 avr. 2020 à 13:04
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `si6test2`
--

-- --------------------------------------------------------

--
-- Structure de la table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `chat` text,
  `id_user` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `chat`
--

INSERT INTO `chat` (`id`, `chat`, `id_user`, `date`) VALUES
(1234618, 'test', 61, '2020-03-31 08:58:59'),
(1234619, '\"r', 61, '2020-03-31 08:59:03'),
(1234620, 'YOO', 48, '2020-03-31 09:00:02'),
(1234621, 'qrreg', 65, '2020-04-04 13:24:27'),
(1234622, 'erf', 65, '2020-04-04 14:58:24'),
(1234623, '\"ér\"é', 65, '2020-04-06 15:04:36'),
(1234624, 'zfa', 65, '2020-04-06 15:10:49'),
(1234625, '\"ré', 65, '2020-04-06 15:14:46'),
(1234626, 'r\"é', 65, '2020-04-06 15:15:05'),
(1234627, 'fze', 64, '2020-04-06 15:15:35'),
(1234628, 'ze', 64, '2020-04-09 09:26:11'),
(1234629, 'zad', 64, '2020-04-09 15:32:14'),
(1234630, '', 61, '2020-04-09 15:39:01'),
(1234631, '', 61, '2020-04-09 15:40:26'),
(1234632, '', 61, '2020-04-09 15:40:27'),
(1234633, '', 61, '2020-04-09 15:40:28'),
(1234634, '', 61, '2020-04-09 15:41:10'),
(1234635, '', 61, '2020-04-09 15:41:12'),
(1234636, 'bkiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiizef\"jijzefijijoezjioezijijoezfijijezfijoeziojjiozeeijijeijefzjfeze', 61, '2020-04-09 15:44:08'),
(1234637, 'zef nj nezf  jjjjjjjjjjjjjjjjjjjjjjj zeffffffffffffffffffffj jjjjjjjjjjzefffffffffffffffffffffffffffffffff ', 61, '2020-04-09 15:44:21'),
(1234638, '', 61, '2020-04-09 15:46:38'),
(1234639, 'zq', 61, '2020-04-09 16:10:20'),
(1234640, 'gfffffffffffffffffffffffffffffffffffffffffffffffffffffftyttttttttttttttttttttttttttttttttttttttttttttttttttooooooooooooooooooo', 61, '2020-04-09 16:12:19'),
(1234641, 'lol', 61, '2020-04-09 16:32:57'),
(1234642, 'ezfhbezhez  zeifiiez zef', 61, '2020-04-09 16:47:57'),
(1234643, 'bonjour et encore merci ', 61, '2020-04-09 16:48:10'),
(1234645, 'j', 61, '2020-04-10 06:49:54'),
(1234646, 'zef', 61, '2020-04-10 12:50:30'),
(1234647, '', 61, '2020-04-10 13:02:49'),
(1234648, 'kllk', 61, '2020-04-10 13:02:52');

-- --------------------------------------------------------

--
-- Structure de la table `connection`
--

CREATE TABLE `connection` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `telephone` varchar(12) NOT NULL,
  `anniv` date NOT NULL,
  `dates` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `connection`
--

INSERT INTO `connection` (`id`, `pseudo`, `email`, `password`, `nom`, `prenom`, `telephone`, `anniv`, `dates`) VALUES
(48, 'benjamin', 'benjamin03.bitoun@gmail.com', '$2y$10$NmtnvbbIcylgVW3LdIeM3OrcF2Z3dk0.B.81TXZnDGuWwqrK7TbB.', 'bitoun', 'benjamin', '0695189323', '2001-03-23', '2020-03-25 08:23:45'),
(49, 'Emilie', 'leieaz@gmail.com', '$2y$10$x.U9icMYaBQEROcyOTvEfuYzIkR4nNY2DyFj4ePpthCIYzwcCAG1.', 'rousselle', 'émilie', '0695121223', '1960-01-01', '2020-03-25 07:02:22'),
(50, 'LOLLOOL', 'aurazdq', '$2y$10$ZObE0xUzB6uKQpU.maazG.Eu/0ch6dS8FwR6cwblFLcnbwIW06IrK', 'roussel', 'aurélie', '8281821882', '1960-01-01', '2020-03-25 07:03:05'),
(51, 'DZAECS', 'ZDACQSDS@gmail.com', '$2y$10$p8sPnUdRhJkTNMBhMs9./euBn5PX7B50RvxQM1D8ltBH4R518dMua', 'ZDAECSVFD', 'AZDZDQC', '2129100299', '1960-01-01', '2020-03-25 07:42:50'),
(52, 'Pablo', 'Pablo@gmail.com', '$2y$10$HHN7iHvewMvsrTQIcAY8FedUGOYNO3Sh654NsIr4aE.8OkPVUiuxC', 'Pepito', 'Pablo', '0695189122', '1960-01-01', '2020-03-25 07:46:14'),
(53, 'Pablo', 'Pablo@gmail.com', '$2y$10$X8ItcdX3N0mjb6JdOZs5SuOq6IYfraEJbjht218vxKhcgqUxFRtqq', 'DeLaVega', 'Pedro', '0721892189', '1960-01-01', '2020-03-25 07:49:38'),
(54, 'Pablo', 'benjamin03.bitounmail.com', '$2y$10$p7AQ3xTHZ5sqL.MRRS9Dd.xLVEOa/lcgKbFMFMoDl9rCEc8asEHKK', 'Pablo', 'Pablo', '0695189888', '1960-01-01', '2020-03-25 07:50:09'),
(55, 'Pepito', 'Pepito@gmail.com', '$2y$10$krmR5dmexf0yCLAgqePpHuoacDJsUJpnXUIKManHrjHmQIVER5rH.', 'Pepito', 'Pepito', '3R2EEFZZZZ', '1960-01-01', '2020-03-25 07:53:41'),
(56, 'Pablo', 'Pablo@gmail.com', '$2y$10$WdaDdwPaehpkJuMipRpc9ep.sED7zBkiiDs//0KJMEKNs23NmekD.', 'Pablo', 'Pablo', '0233289832', '1960-01-01', '2020-03-25 07:54:39'),
(57, 'Pedro', 'Pedro@gmail.com', '$2y$10$obr7FlskOm3nqZY0ZMcr1eJeNIxvthPkI3d88.N8I0Eqx/sxDOG26', 'Pedro', 'Pedro', '06951893ds', '1960-01-01', '2020-03-25 08:01:38'),
(58, 'PEPITOL', 'PEPITOL@gmail.com', '$2y$10$5u4ZQyezcy07IqaGaPuxJeZdSJPaMmM6DqL80dL5.dHKjUClzv57e', 'PEPITOL', 'PEPITOL', '0692818218', '1960-01-01', '2020-03-25 08:16:33'),
(59, 'PEPITOL', 'PEPITOL@gmail.com', '$2y$10$1f0vL0NHxLVORookP2Vhx.udMWY/YONa/.kal5TA1POPg4nJitn8y', 'PEPITOL', 'PEPITOL', '0618821821', '1960-01-01', '2020-03-25 08:18:24'),
(60, 'PEPITOLZ', 'PEPITOLZ@gmail.com', '$2y$10$fu8HSnizW4bBn.r9s/buCeKNq.EUMJslaeBN4ugDZal9eCfXPLH1i', 'PEPITOLZ', 'PEPITOLZ', '0728327889', '1960-01-01', '2020-03-25 08:19:53'),
(61, 'TRUELLE', 'TRUELLE@gmail.com', '$2y$10$xWQYmsJH6Awe9X8sHewu1.fieSg62JvNBLgcplZEfbnGcX4ttMKWu', 'TRUELLE', 'TRUELLE', '0625189323', '1960-01-01', '2020-03-27 07:26:24'),
(62, 'hannah.b', 'hannah.bitoun@gmail.com', '$2y$10$riEQ9kcf1HkcwrhfoN8wr.n92AX9IGXHLpf8/zYl2FFAAg25Xf80y', 'BITOUN', 'Hannah', '0660751904', '1992-05-10', '2020-03-29 15:59:29'),
(63, 'PAELLA', 'PAELLA@gmail.com', '$2y$10$kNyIJX841E19Xn97mbtjK.0UtoRMSXXWfc6e3mN9xh1mOrgAD7U0q', 'PAELLA', 'PAELLA', '0695189323', '1960-01-01', '2020-04-03 19:40:14'),
(64, 'PAOLO', 'PAOLO@GMAIL.COM', '$2y$10$n5Y9zpDGeJsLNOpMQnTT/uQR0uwc5gR9uuRS4BZ5.G363GlKEJVr6', 'PAOLO', 'PAOLO', '0695189323', '1960-01-01', '2020-04-04 12:03:28'),
(65, 'MEXICO', 'MEXICO@gmail.com', '$2y$10$4lGGm99S88MXu8pJs429DeAiNBuspASz4ivkEyZKp.A1SFZMC.s.C', 'MEXICO', 'MEXICO', '0695189323', '1960-01-01', '2020-04-04 15:51:26'),
(66, 'HOLLA', 'HOLLA@gmail.com', '$2y$10$RayVZzMkNS13fNo5Bus0me7JCjiWtMe.xhV/14D8ElRvjF2tp58oq', 'HOLLA', 'HOLLA', '0695189323', '1960-01-01', '2020-04-04 13:02:27'),
(67, 'ESTA', 'ESTA@gmail.com', '$2y$10$OlAx2tYhYh1Mze9NSwRfMuG/kZr38EZKn4UYbcOa/SBVaMpwPMhMC', 'ESTA', 'ESTA', '0695189323', '1960-01-01', '2020-04-04 15:46:51'),
(68, 'gloglo', 'aaa@aaa.com', '$2y$10$K22cCC1IxPmjsY1.QuBXK.E0piqy5L.QUPPdp/F7MSAXpUqD.Gvo6', 'baguette', 'globule', '0695189323', '1975-02-16', '2020-04-05 16:37:28'),
(69, 'DIDA', 'AZIZA@gmail.com', '$2y$10$JO5oWVe/XkgZGdYTCU6RLuTfHAQuhkLiW3i2LxjTkCbZo8EbIrkJ.', 'AZIZA', 'HEMISSI', '0695121123', '2020-01-01', '2020-04-06 14:59:38'),
(70, 'EE2A2A2A2A2A2A2A', 'benjamin03.bitoun@gmail.com', '$2y$10$nDc2PrL52FS4LqHPBOx1weQ9IwqYuu1P3lQwpoNRQsesQLZK0NAOi', 'TRUELLE', 'TRUELLE', '0693229323', '1960-01-01', '2020-04-09 17:22:03'),
(71, 'zinebZAD', 'TRUELLE@gmail.com', '$2y$10$Kaklwqs6nqDBX31Ta3NPr.Yr2aErb9qpXr9FLKMM.7YXtfp2JX0fK', 'TRUELLE', 'TRUELLE', '0695182323', '1960-01-01', '2020-04-09 17:22:51'),
(72, 'TRUELLETRUELLE', 'TRUELLE@gmail.com', '$2y$10$tlydxsFylNTGk3hYaMuE..Cgk1N2PvFG1JvYovrl.5V4Q3LlCpjZy', 'TRUELLE', 'TRUELLE', '0611189323', '1960-01-01', '2020-04-09 17:23:59'),
(73, 'TRUELLEZ2', 'benjamin03.bitoun@gmail.com', '$2y$10$fgsCji0EaGQzIRQBcwem4eeBdXBMiA2IpWM4vMcKvXlVrL2PUpCZG', 'TRUELLE', 'TRUELLE', '0695189222', '1960-01-01', '2020-04-09 17:28:15');

-- --------------------------------------------------------

--
-- Structure de la table `medecin`
--

CREATE TABLE `medecin` (
  `id` int(11) NOT NULL,
  `medecin` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `medecin`
--

INSERT INTO `medecin` (`id`, `medecin`) VALUES
(1, 'BILIEN');

-- --------------------------------------------------------

--
-- Structure de la table `rdv`
--

CREATE TABLE `rdv` (
  `id` int(11) NOT NULL,
  `Services` varchar(255) NOT NULL,
  `lieux` varchar(255) NOT NULL,
  `Personne` varchar(255) NOT NULL,
  `debut` datetime NOT NULL,
  `fin` datetime NOT NULL,
  `Message` text NOT NULL,
  `id_utilisateur` int(11) NOT NULL,
  `creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `rdv`
--

INSERT INTO `rdv` (`id`, `Services`, `lieux`, `Personne`, `debut`, `fin`, `Message`, `id_utilisateur`, `creation`) VALUES
(35, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-03-27 12:21:00', '2020-03-27 12:51:00', 'ZAD', 48, '2020-03-26 22:21:08'),
(36, 'Simple consultation', 'Livry-Gargan', 'Adult', '2020-03-29 21:12:00', '2020-03-29 21:12:00', 'DZA', 48, '2020-03-26 22:23:24'),
(37, 'Autre Services', 'Livry-Gargan', 'Enfants', '2020-03-20 12:21:00', '2020-03-20 12:21:00', 'ZDA', 48, '2020-03-26 22:23:48'),
(38, 'Autre Services', 'Livry-Gargan', 'Enfants', '2020-03-28 12:21:00', '2020-03-28 12:51:00', 'E', 48, '2020-03-26 22:24:20'),
(39, 'Autre Services', 'azdezgrez&quot;refs', 'Enfants', '2020-04-03 01:21:00', '2020-04-03 01:51:00', 'UNNZUIZEUIEZ', 48, '2020-03-26 22:25:08'),
(40, 'Neurologie', 'Livry-Gargan', 'Enfants', '2020-03-28 04:55:00', '2020-03-28 05:25:00', 'JJ', 48, '2020-03-27 07:13:05'),
(41, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-03-27 23:49:00', '2020-03-27 23:49:00', '', 48, '2020-03-27 07:13:45'),
(42, 'Simple consultation', 'Livry-Gargan', 'Adult', '2020-03-27 23:45:00', '2020-03-27 23:45:00', '...', 48, '2020-03-27 07:14:09'),
(43, 'Neurologie', 'Livry-Gargan', 'Adult', '2020-03-27 23:47:00', '2020-03-27 23:47:00', 'KJJNJ', 48, '2020-03-27 07:14:39'),
(44, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-03-28 03:54:00', '2020-03-28 04:24:00', 'JKJ', 48, '2020-03-27 07:15:00'),
(45, 'Simple consultation', 'Livry-Gargan', 'Adult', '2020-03-27 23:48:00', '2020-03-27 23:48:00', 'JOI', 48, '2020-03-27 07:16:00'),
(46, 'Neurologie', 'Livry-Gargan', 'Adult', '2020-03-27 23:47:00', '2020-03-27 23:47:00', 'JKKJ', 48, '2020-03-27 07:16:59'),
(47, 'Neurologie', 'Livry-Gargan', 'Adult', '2020-03-27 23:47:00', '2020-03-27 23:47:00', 'JKKJ', 48, '2020-03-27 07:19:07'),
(48, 'Neurologie', 'Livry-Gargan', 'Adult', '2020-03-27 23:47:00', '2020-03-27 23:47:00', 'JKKJ', 48, '2020-03-27 07:19:24'),
(49, 'Neurologie', 'Livry-Gargan', 'Adult', '2020-03-27 23:47:00', '2020-03-27 23:47:00', 'JKKJ', 48, '2020-03-27 07:22:13'),
(50, 'Neurologie', 'Livry-Gargan', 'Adult', '2020-03-27 23:47:00', '2020-03-28 00:17:00', 'JKKJ', 48, '2020-03-27 07:24:11'),
(51, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-03-31 23:32:00', '2020-04-01 00:02:00', 'SD', 48, '2020-03-27 07:24:48'),
(52, 'Simple consultation', 'Livry-Gargan', 'Enfants', '2020-03-27 21:32:00', '2020-03-27 22:02:00', 'csde', 61, '2020-03-27 07:26:52'),
(53, 'Neurologie', 'Livry-Gargan', 'Adult', '2020-03-28 10:00:00', '2020-03-28 10:30:00', 'A l\'heure svp !!', 61, '2020-03-27 21:00:17'),
(54, 'Cardiologie', 'Livry-Gargan', 'Enfants', '2020-03-29 21:21:00', '2020-03-29 21:51:00', 'EZ', 61, '2020-03-28 12:57:38'),
(55, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-03-28 14:05:00', '2020-03-28 14:35:00', 'R', 61, '2020-03-28 12:59:12'),
(56, 'Simple consultation', 'Livry-Gargan', 'Adult', '2020-03-29 23:32:00', '2020-03-30 00:02:00', 'FEZ', 61, '2020-03-29 15:26:16'),
(57, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-03-29 19:09:00', '2020-03-29 19:39:00', 'D', 61, '2020-03-29 15:40:08'),
(58, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-02-23 21:02:00', '2020-02-23 21:32:00', '2E', 61, '2020-03-29 15:41:43'),
(59, 'Simple consultation', 'Livry-Gargan', 'Adult', '2020-05-10 17:30:00', '2020-05-10 17:30:00', '', 62, '2020-03-29 16:02:10'),
(60, 'Simple consultation', 'Livry-Gargan', 'Adult', '2020-05-10 17:30:00', '2020-05-10 17:30:00', 'VB', 62, '2020-03-29 16:02:19'),
(61, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-04-01 04:54:00', '2020-04-01 05:24:00', 'F', 62, '2020-03-29 16:03:00'),
(62, 'Simple consultation', 'Livry-Gargan', 'Adult', '2020-11-11 18:00:00', '2020-11-11 18:30:00', 'VCGHVB', 62, '2020-03-29 16:03:30'),
(63, 'Autre Services', '9 allée lucien michard', 'Adult', '2020-04-04 12:21:00', '2020-04-04 12:51:00', 'kkll', 61, '2020-03-30 07:46:27'),
(64, 'Cardiologie', 'Livry-Gargan', 'Adult', '2020-04-01 03:32:00', '2020-04-01 04:02:00', 'dsw', 61, '2020-03-30 13:31:01'),
(65, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-04-03 21:21:00', '2020-04-03 21:51:00', 'EZFF', 61, '2020-03-30 13:33:49'),
(66, 'Neurologie', 'Livry-Gargan', 'Adult', '2020-04-02 12:12:00', '2020-04-02 12:42:00', 'REGGRE', 61, '2020-03-30 13:56:19'),
(67, 'Autre Services', '', 'Adult', '2020-04-02 15:00:00', '2020-04-02 15:30:00', 'erre', 61, '2020-03-30 15:20:12'),
(68, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-04-16 16:55:00', '2020-04-16 17:25:00', 'TA MERE !!', 61, '2020-03-30 15:45:16'),
(69, 'Autre Services', '', 'Adult', '2020-04-03 12:21:00', '2020-04-03 12:51:00', 'E23A', 61, '2020-03-31 08:43:37'),
(70, 'Autre Services', '', 'Adult', '2020-04-03 21:21:00', '2020-04-03 21:51:00', '', 61, '2020-03-31 08:43:46'),
(71, 'Autre Services', 'Livry-Gargan', 'Adult', '2900-04-01 12:21:00', '2900-04-01 12:51:00', 'DEZ', 48, '2020-03-31 09:55:39'),
(72, 'Simple consultation', 'Livry-Gargan', 'Adult', '2020-04-05 12:21:00', '2020-04-05 12:51:00', 'EZF', 48, '2020-03-31 10:00:37'),
(73, 'Simple consultation', 'Livry-Gargan', 'Adult', '2020-03-27 21:21:00', '2020-03-27 21:51:00', '3R', 48, '2020-03-31 19:04:34'),
(74, 'Simple consultation', 'Livry-Gargan', 'Adult', '2020-03-27 21:21:00', '2020-03-27 21:51:00', '3R', 48, '2020-03-31 19:04:50'),
(75, 'Neurologie', 'Livry-Gargan', 'Adult', '2020-04-02 21:21:00', '2020-04-02 21:51:00', '2E1', 48, '2020-03-31 19:17:15'),
(76, 'Neurologie', 'Livry-Gargan', 'Adult', '2020-04-02 21:21:00', '2020-04-02 21:51:00', '2E1', 48, '2020-03-31 19:17:33'),
(77, 'Cardiologie', 'Livry-Gargan', 'Adult', '2020-04-02 12:21:00', '2020-04-02 12:51:00', 'EZ', 48, '2020-03-31 19:18:54'),
(78, 'Neurologie', 'Livry-Gargan', 'Adult', '2020-04-03 12:02:00', '2020-04-03 12:32:00', 'E', 48, '2020-03-31 19:21:34'),
(79, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-03-28 12:21:00', '2020-03-28 12:51:00', 'ZEF', 48, '2020-03-31 19:22:56'),
(80, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-04-02 13:03:00', '2020-04-02 13:33:00', 'F3Z', 48, '2020-03-31 19:25:23'),
(81, 'Ophthalmologie', 'jnjnnnkmk', 'Enfants', '2020-04-03 04:55:00', '2020-04-03 05:25:00', 'nkkkk', 48, '2020-03-31 19:58:27'),
(82, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-04-17 21:21:00', '2020-04-17 21:51:00', 'zadq', 48, '2020-04-03 09:25:54'),
(83, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-04-25 12:21:00', '2020-04-25 12:51:00', 'E21', 48, '2020-04-03 09:28:32'),
(84, 'Simple consultation', '21', 'Adult', '2020-04-21 21:21:00', '2020-04-21 21:51:00', 'ds', 48, '2020-04-03 17:08:25'),
(85, 'Simple consultation', '21', 'Adult', '2020-04-21 21:21:00', '2020-04-21 21:51:00', 'ds', 48, '2020-04-03 17:08:55'),
(86, 'Autre Services', 'Livry-Gargan', 'Adult', '2222-02-21 21:21:00', '2222-02-21 21:51:00', 'E21', 48, '2020-04-03 17:11:47'),
(87, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-04-25 12:01:00', '2020-04-25 12:31:00', 'azad', 48, '2020-04-03 17:18:39'),
(88, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-04-24 12:21:00', '2020-04-24 12:51:00', '21E', 48, '2020-04-03 17:20:13'),
(89, 'Autre Services', 'JE TESTEEAZ', 'Adult', '2020-04-25 21:21:00', '2020-04-25 21:51:00', '', 48, '2020-04-03 19:12:33'),
(90, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-04-01 12:21:00', '2020-04-01 12:51:00', 'ED1', 65, '2020-04-04 14:48:02'),
(91, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-04-30 12:21:00', '2020-04-30 12:51:00', '21E', 65, '2020-04-04 14:48:19'),
(92, 'Neurologie', 'Livry-Gargan', 'Adult', '2020-04-25 22:01:00', '2020-04-25 22:31:00', 'E21', 67, '2020-04-04 15:43:03'),
(93, 'Autre Services', '', 'Adult', '2020-04-06 11:11:00', '2020-04-06 11:41:00', '', 65, '2020-04-05 11:19:31'),
(94, 'Autre Services', '', 'Adult', '2020-04-06 11:11:00', '2020-04-06 11:41:00', '', 65, '2020-04-05 11:19:39'),
(95, 'Neurologie', '', 'Adult', '2020-04-06 11:11:00', '2020-04-06 11:41:00', '', 65, '2020-04-05 11:19:52'),
(96, 'Ophthalmologie', '', 'Adult', '2020-04-06 11:11:00', '2020-04-06 11:41:00', '', 65, '2020-04-05 11:20:02'),
(97, 'Cardiologie', '', 'Adult', '2020-04-06 11:11:00', '2020-04-06 11:41:00', 'D', 65, '2020-04-05 11:20:34'),
(98, 'Autre Services', '', 'Adult', '2020-04-06 11:11:00', '2020-04-06 11:41:00', '', 65, '2020-04-05 15:05:28'),
(99, 'Dentaire', 'zdaq', 'Adult', '2020-04-06 11:11:00', '2020-04-06 11:41:00', 'A2Z', 65, '2020-04-05 15:05:50'),
(100, 'Autre Services', 'baguettecity', 'Adult', '2020-12-09 01:04:00', '2020-12-09 01:34:00', 'Globulle cherche amis', 68, '2020-04-05 16:39:08'),
(101, 'Autre Services', 'Livry-Gargan', 'Enfants', '2020-04-07 17:00:00', '2020-04-07 17:30:00', 'FF', 69, '2020-04-06 15:00:31'),
(102, 'Autre Services', 'Livry-Gargan', 'Adult', '2020-03-31 12:21:00', '2020-03-31 12:51:00', 'EZF', 69, '2020-04-06 15:01:13'),
(103, 'Autre Services', 'zadez', 'Adult', '2020-04-23 21:21:00', '2020-04-23 21:51:00', 'azd', 65, '2020-04-06 15:02:10'),
(104, 'Dentaire', 'Livry-Gargan', 'Adult', '2020-04-10 03:43:00', '2020-04-10 04:13:00', ', jkb', 61, '2020-04-09 16:54:40');

-- --------------------------------------------------------

--
-- Structure de la table `reclamation`
--

CREATE TABLE `reclamation` (
  `id` int(11) NOT NULL,
  `sujet` varchar(255) NOT NULL,
  `Message` text NOT NULL,
  `id_utilisateur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `reclamation`
--

INSERT INTO `reclamation` (`id`, `sujet`, `Message`, `id_utilisateur`) VALUES
(1, 'jjk', 'hjhj', 36),
(2, 'aed', 'azd', 36),
(3, 'azjezbjzejbbjze', 'azbjzefbjzejbzebjez', 41),
(4, 'zef', 'ze', 39),
(5, '', '', 48),
(6, '', '', 65),
(7, 'azd', 'zda', 65),
(8, '', '', 65),
(9, '', '', 65),
(10, 'se', 'efz', 65),
(11, 'ez', 'efz', 65),
(12, 'fez', 'ezf', 65),
(13, 'azd', 'zf&quot;e', 65),
(14, 'efzez', 'f', 65),
(15, '', '', 65),
(16, '', '', 65),
(17, '', '', 64),
(18, 'qcs', 'scq', 64);

-- --------------------------------------------------------

--
-- Structure de la table `rendezvous`
--

CREATE TABLE `rendezvous` (
  `id` int(11) NOT NULL,
  `dates` datetime NOT NULL,
  `lieux` varchar(100) NOT NULL,
  `medecin` varchar(50) NOT NULL,
  `Adults` int(1) NOT NULL,
  `enfant` int(1) NOT NULL,
  `id_utilisateur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `rendezvous`
--

INSERT INTO `rendezvous` (`id`, `dates`, `lieux`, `medecin`, `Adults`, `enfant`, `id_utilisateur`) VALUES
(9, '2019-12-12 00:00:00', 'Livry-Gargan', '1£A', 1, 0, 0),
(10, '2020-03-23 17:09:00', 'Livry-Gargan', 'bilien', 1, 0, 0),
(11, '2888-03-23 16:00:00', 'Livry-Gargan', 'bilien', 1, 0, 0),
(12, '2089-03-23 23:04:00', 'Livry-Gargan', '1£A', 1, 0, 21),
(13, '2222-02-22 04:44:00', 'Livry-Gargan', 'bilien', 1, 0, 21),
(14, '2211-08-23 20:03:00', 'Livry-Gargan', 'Arouf', 0, 1, 28),
(15, '1111-03-23 21:11:00', 'Livry-Gargan', 'bilien', 1, 0, 28),
(16, '1111-11-11 11:11:00', 'Livry-Gargan', 'AAAAAAAAAAAAAAAA', 1, 0, 28),
(17, '2001-03-23 21:12:00', 'Livry-Gargan', '', 1, 0, 29),
(18, '2222-11-11 12:21:00', 'Livry-Gargan', 'Arouf', 1, 0, 29),
(19, '2020-05-30 16:00:00', 'Livry-Gargan', 'bilien', 1, 0, 32),
(20, '2222-11-11 11:11:00', '11', '', 1, 0, 34);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `connection`
--
ALTER TABLE `connection`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pseudo` (`pseudo`,`email`,`telephone`);

--
-- Index pour la table `medecin`
--
ALTER TABLE `medecin`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rdv`
--
ALTER TABLE `rdv`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reclamation`
--
ALTER TABLE `reclamation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rendezvous`
--
ALTER TABLE `rendezvous`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1234649;

--
-- AUTO_INCREMENT pour la table `connection`
--
ALTER TABLE `connection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT pour la table `medecin`
--
ALTER TABLE `medecin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `rdv`
--
ALTER TABLE `rdv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT pour la table `reclamation`
--
ALTER TABLE `reclamation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `rendezvous`
--
ALTER TABLE `rendezvous`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
