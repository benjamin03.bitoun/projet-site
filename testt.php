<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}


.close {
  cursor: pointer;
  position: absolute;
  top: 50%;
  right: 0%;
  padding: 12px 16px;
  transform: translate(0%, -50%);
}

.close:hover {background: #bbb;}
</style>
</head>
<body>

<h2>Closable List Items</h2>
<p>Click on the "x" symbol to the right of the list item to close/hide it.</p>

<form  onsubmit='return confirmation();' method="POST">
<ul>
  <li>Adele</li>
  <li>Agnes<span class="close"><button type="submit" class="btn btn-two">&times;</button></span></li>

  <li>Billy<span class="close">&times;</span></li>
  <li>Bob<span class="close">&times;</span></li>

  <li>Calvin<span class="close">&times;</span></li>
  <li>Christina<span class="close">&times;</span></li>
  <li>Cindy</li>
</ul></form>




<script>
var closebtns = document.getElementsByClassName("close");
var i;

for (i = 0; i < closebtns.length; i++) {
  closebtns[i].addEventListener("click", function() {
    this.parentElement.style.display = 'none';
  });
}

function confirmation() {
var str = 'Etes-vous  sur de Vouloir vous deconnecter  ?';
return confirm(str);
}
</script>

<div class="box-2">
    
    
  </div>

</form>



</body>
</html>
