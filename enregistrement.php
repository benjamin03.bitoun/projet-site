

<!DOCTYPE html>
<html>
<head>
  <title>Inscription</title>
      <link rel="shortcut icon" type="image/x-icon" href="img/logo.ico">
  <meta http-equiv="refresh">
<link rel="stylesheet" type="text/css" href="css/enregistrement.css" />
</head>






<body>

<form id="regForm" action="enregistrement2.php" method="post">
  <h1>Inscription:</h1>
  <!-- One "tab" for each step in the form: -->
  <div class="tab">Nom :
    <p><input placeholder="Votre prénom..."type="text"maxlength="50" oninput="this.className = ''" name="prenom"></p>
    <p><input placeholder="Votre nom..." type="text" maxlength="50" oninput="this.className = ''" name="nom"></p>
    <button type="button" onclick="window.location.href = 'index.php';">Retour</button>
  </div>
  <div class="tab">Information personnelle:
    <p><input placeholder="E-mail..." oninput="this.className = ''" name="email">
</p>
    <p><input placeholder="téléphone..." maxlength="10" pattern="[0-9]{10}" oninput="this.className = ''" name="telephone"><div class="error"><?php if(isset($error_phone)){echo $error_phone; } ?></div>  </p>


    <p>Veuillez entrée votre date de naissance</p>
    <p><select name="jour">
<?php

$i=1;
while($i<32){
?><option name="jour" value="<?php echo $i; ?>" /><?php echo $i; ?></option><?php
$i++;
}
?></select>
<select name="mois">
<?php
$j=1;
while($j<13){
?><option name="mois" value="<?php echo $j; ?>" /><?php echo $j; ?></option><?php
$j++;
}
?></select>
<select name="annee">
     <?php $date=date("Y");

$date = $date+1;
$a=1960;
while($a<$date){
?><option name="annee" value="<?php echo $a; ?>" /><?php echo $a; ?></option><?php
$a++;
}
?></select>  </div>
  </div>
  <div class="tab">Information de connexion:
    <p><input placeholder="Pseudo..."  maxlength="20" oninput="this.className = ''" name="pseudo"></p>
    <p><input placeholder="Mot de passe..." maxlength="30" oninput="this.className = ''" name="password" type="password"></p>
    <p><input placeholder="Confirmer votre mot de passe" maxlength="30" oninput="this.className = ''" name="cpassword" type="password"></p>
  </div>
  <div style="overflow:auto;">
    <div style="float:right;">
      <button type="button" id="prevBtn" onclick="nextPrev(-1)">Retour</button>
      <button type="button" id="nextBtn" onclick="nextPrev(1)">Suivant</button>
    </div>
  </div>
  <!-- Circles which indicates the steps of the form: -->
  <div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>

  </div>
</form>

<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "S'inscrire";
  } else {
    document.getElementById("nextBtn").innerHTML = "Suivant";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
</script>

</body>

</html>