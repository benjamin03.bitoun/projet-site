<?php 



require('config.php');

  

  if (!isset($_POST['prenom']) || strlen($_POST['prenom']) < 3 ||  strlen($_POST['prenom']) > 25) {
    header('Location: enregistrement.php?msg="Erreur dans les informations (prenom doit etre compris entre 3 et 25 caractere)"');
    exit;
  } else {
    $prenom = htmlspecialchars($_POST['prenom']);
  }


  if (!isset($_POST['nom']) || strlen($_POST['nom']) < 3 ||  strlen($_POST['nom']) > 25) {
    header('Location: enregistrement.php?msg="Erreur dans les informations (nom)"');
    exit;
  } else {
    $nom = htmlspecialchars($_POST['nom']);
  }










// La fonction preg_match prend en paramètre l'expression régulière et le numéro de téléphone entré et renvoi true si le numéro est valide, false sinon
if (preg_match('#(0|\+33)[1-9]( *[0-9]{2}){4}#', $_POST['telephone'])) {
  echo "Le numéro de téléphone entré est correct.";
  $telephone = htmlspecialchars($_POST['telephone']);
  // Ajout du numéro de téléphone à la base de donnée
} else {
      $message = " ERREUR ";
      header('Location: enregistrement.php?msg="Erreur dans les informations (telephone)"');
    exit;
  
  // On ne peut pas ajouter le numéro à la base de donnée
}

  
    if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
    echo "L'adresse e-mail est valide";
    $mail = $_POST['email'];
  }else{
    echo "L'adresse e-mail n'est pas valide";
       header('Location: enregistrement.php?msg="Erreur dans les informations (mail)"');
    exit;}



$anniv=$_POST['annee']."-".$_POST['mois']."-".$_POST['jour'];


  if (!isset($_POST['pseudo']) || strlen($_POST['pseudo']) < 3 ||  strlen($_POST['pseudo']) > 19) {
    header('Location: index.php?msg="Erreur dans les informations (pseudo)"');
    exit;
  } else {
    $pseudo = htmlspecialchars($_POST['pseudo']);
  }



  if (!isset($_POST['password']) || strlen($_POST['password']) < 3 || strlen($_POST['password']) > 255) {
    header('Location: index.php?msg="Erreur dans les informations (password)"');
    exit;
  } else {

    $password = $_POST['password'];
  }


    if (!isset($_POST['cpassword']) || strlen($_POST['cpassword']) < 3 || strlen($_POST['cpassword']) > 255) {
    header('Location: index.php?msg="Erreur dans les informations (cpassword)"');
    exit;
  } else {
    $cpassword = $_POST['cpassword'];
  }

;
$hashed_password = password_hash($_POST["password"],PASSWORD_DEFAULT);

 $select = $bdd->query('SELECT * FROM connection ');
 while($valeur = $select->fetch()){
if($mail == $valeur[2]){
  header('Location:enregistrement.php?msg="Cette email est déja utilisée"');
  exit();
}

if($telephone == $valeur[6]){
  header('Location:enregistrement.php?msg="Ce numéro de téléphone est déja utilisée"');
  exit();
}

if($pseudo == $valeur[1]){
  header('Location:enregistrement.php?msg="Ce pseudo  est déja utilisée"');
  exit();
}


}
    $q = 'INSERT INTO connection (pseudo, email, password, nom, prenom, telephone, anniv) VALUES (?, ?, ?, ?, ?, ?, ?)';
    $req = $bdd->prepare($q);
    $req->execute([$pseudo, $mail, $hashed_password, $nom, $prenom, $telephone,$anniv]);






?>


<?php
  header('Location:Acceuil.php');
  exit();
?>



