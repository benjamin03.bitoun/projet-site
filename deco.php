<?php 
session_start();

require('config.php');
// Suppression des variables de session et de la session
$_SESSION = array();
session_destroy();

// Suppression des cookies de connexion automatique
setcookie('pseudo', '');
setcookie('password', '');
header('location:index.php');
?>